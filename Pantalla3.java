package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla3 {

	private JFrame frame;
	private JTextField txt;
	private JLabel lblResultado;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla3 window = new Pantalla3();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla3() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(127, 255, 212));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblIngresarNumeroDel = new JLabel("INGRESAR NUMERO DEL MES");
		lblIngresarNumeroDel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblIngresarNumeroDel.setBounds(31, 37, 198, 22);
		frame.getContentPane().add(lblIngresarNumeroDel);
		
		txt = new JTextField();
		txt.setBounds(254, 39, 116, 22);
		frame.getContentPane().add(txt);
		txt.setColumns(10);
		
		JButton btnCalcular = new JButton("CALCULAR");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int numero = Integer.parseInt(txt.getText());
				
				if (numero==1)
					lblResultado.setText("ENERO TIENE 31 DIAS");
				else if (numero ==2)
					lblResultado.setText("FEBRERO TIENE 28 DIAS");
				else if (numero == 3)
					lblResultado.setText("MARZO TIENE 31 DIAS");
				else if (numero == 4)
					lblResultado.setText("ABRIL TIENE 30 DIAS");
				else if (numero ==5)
					lblResultado.setText("MAYO TIENE 31 DIAS");
				else if (numero == 6)
					lblResultado.setText("JUNIO TIENE 30 DIAS");
				else if (numero == 7)
					lblResultado.setText("JULIO TIENE 31 DIAS");
				else if (numero ==8)
					lblResultado.setText("AGOSTO TIENE 31 DIAS");
				else if (numero == 9)
					lblResultado.setText("SEPTIEMBRE TIENE TIENE 30 DIAS");
				else if (numero == 10)
					lblResultado.setText("OCTUBRE TIENE 31 DIAS");
				else if (numero ==11)
					lblResultado.setText("NOVIEMBRE TIENE 30 DIAS");
				else if (numero == 12)
					lblResultado.setText("DICIEMBRE TIENE 31 DIAS");
				
			}
		});
		btnCalcular.setForeground(new Color(199, 21, 133));
		btnCalcular.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnCalcular.setBounds(166, 99, 97, 25);
		frame.getContentPane().add(btnCalcular);
		
		lblResultado = new JLabel("");
		lblResultado.setOpaque(true);
		lblResultado.setBackground(new Color(255, 182, 193));
		lblResultado.setBounds(107, 165, 224, 30);
		frame.getContentPane().add(lblResultado);
	}
}
