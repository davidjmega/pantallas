package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla2 {

	private JFrame frame;
	private JTextField txt;
	private JLabel lblResultado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla3 window = new Pantalla2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(127, 255, 212));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNumero = new JLabel("NUMERO");
		lblNumero.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNumero.setForeground(new Color(0, 0, 0));
		lblNumero.setBounds(108, 34, 70, 25);
		frame.getContentPane().add(lblNumero);
		
		txt = new JTextField();
		txt.setBounds(217, 38, 116, 22);
		frame.getContentPane().add(txt);
		txt.setColumns(10);
		
		JButton btnCalcular = new JButton("CALCULAR");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int numero = Integer.parseInt(txt.getText());
				
				if (numero%2==0)
					lblResultado.setText("ES PAR");
				else lblResultado.setText("ES IMPAR");
				
			}
		});
		btnCalcular.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnCalcular.setBounds(92, 123, 112, 31);
		frame.getContentPane().add(btnCalcular);
		
		lblResultado = new JLabel("RESULTADO");
		lblResultado.setOpaque(true);
		lblResultado.setBackground(new Color(255, 182, 193));
		lblResultado.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblResultado.setForeground(new Color(0, 0, 0));
		lblResultado.setBounds(235, 121, 88, 31);
		frame.getContentPane().add(lblResultado);
	}

}
